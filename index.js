/*-----------------------------------------------------------------------------
 * @package:    Kwaeri User Experience
 * @author:     Richard B Winters
 * @copyright:  2015-2018 Massively Modified, Inc.
 * @license:    Apache-2.0
 * @version:    0.1.13
 *---------------------------------------------------------------------------*/

// Import the kwaeri User Experience Library from the dist directory
export { kux } from './dist/js/src/kux';