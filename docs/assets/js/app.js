/*-----------------------------------------------------------------------------
 * @package;    Kwaeri User Experience - Sandbox/Demo
 * @author:     Richard B Winters
 * @copyright:  2015-2018 Massively Modified, Inc.
 * @license:    Apache-2.0
 * @version:    0.1.13
 *---------------------------------------------------------------------------*/


// INCLUDES - Use ES5 Syntax to write our application code:


// Prep a dom element for displaying type output to the end user:
var childElementForType = document.createElement( 'p' );

// And begin our feature demonstration with a type() demo:
var anArray = ['First', 'Second'];

// Define output for the type test:
childElementForType.innerHTML = 'Type of anArray = ' + kwdt.type( anArray ) + '.';

// Define the type test output container:
var typeDumpster = document.querySelector( '[data-purpose="type-dumpster"]' );

// Log the result:
typeDumpster.appendChild( childElementForType );

// And give it a special color:
kwdt.addClass( childElementForType, 'yellow-text' );
kwdt.addClass( childElementForType, 'console' );

// Prep dom elements for displaying queue output to the end user:
var childElementForQueue1 = document.createElement( 'p' ),
    childElementForQueue2 = document.createElement( 'p' ),
    childElementForQueue3 = document.createElement( 'p' ),
    childElementForQueue4 = document.createElement( 'p' );

// addClass Demo: Add a special font color to the output:
kwdt.addClass( childElementForQueue1, 'blue-text' );
kwdt.addClass( childElementForQueue1, 'console' );
kwdt.addClass( childElementForQueue2, 'blue-text' );
kwdt.addClass( childElementForQueue2, 'console' );
kwdt.addClass( childElementForQueue3, 'blue-text' );
kwdt.addClass( childElementForQueue3, 'console' );
kwdt.addClass( childElementForQueue4, 'blue-text' );
kwdt.addClass( childElementForQueue4, 'console' );

// Define output for the queue system
childElementForQueue1.innerHTML= 'Queued function 1 called!';
childElementForQueue2.innerHTML= 'Queued function 2 called!';
childElementForQueue3.innerHTML= 'Queued function 3 called!';
childElementForQueue4.innerHTML= 'Queue finished callback called!';

// Define the output target for the queue system test
var queueDumpster = document.querySelector( '[data-purpose="queue-dumpster"]' );

// Define the many callbacks we'll use to demonstrate the queue system
var qf1 = function(){ queueDumpster.appendChild( childElementForQueue1 ); },
    qf2 = function(){ queueDumpster.appendChild( childElementForQueue2 ); },
    qf3 = function(){ queueDumpster.appendChild( childElementForQueue3 ); },
    qcb = function(){ queueDumpster.appendChild( childElementForQueue4 ); };

// queue Demo
kwdt.queue()( 5000 )( qf1 )( 5000 )( qf2 )( 5000 )( qf3 )( 'callback', qcb )();

// script Demo
kwdt.script( { file: 'test-script.js' } );