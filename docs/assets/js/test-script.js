/*-----------------------------------------------------------------------------
 * @package;    Kwaeri User Experience - Sandbox/Demo
 * @author:     Richard B Winters
 * @copyright:  2015-2018 Massively Modified, Inc.
 * @license:    Apache-2.0
 * @version:    0.1.13
 *---------------------------------------------------------------------------*/


( function()
    {
        var scriptDumpster = document.querySelector( '[data-purpose="script-dumpster"]' );

        var childElementForScript = document.createElement( 'p' );

        childElementForScript.innerHTML= 'We have successfully loaded an external script!';

        scriptDumpster.appendChild( childElementForScript );

        kwdt.addClass( childElementForScript, 'green-text' );
        kwdt.addClass( childElementForScript, 'console' );
    }
)();