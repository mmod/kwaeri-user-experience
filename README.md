# kwaeri-user-experience (kux)

[![pipeline status](https://gitlab.com/mmod/kwaeri-user-experience/badges/master/pipeline.svg)](https://gitlab.com/mmod/kwaeri-user-experience/commits/master)  [![coverage report](https://gitlab.com/mmod/kwaeri-user-experience/badges/master/coverage.svg)](https://mmod.gitlab.io/kwaeri-user-experience/coverage/)  [![CLI Best Practices](https://bestpractices.coreinfrastructure.org/projects/1837/badge)](https://bestpractices.coreinfrastructure.org/projects/1837)

> WARNING! kux is not ready for production yet. We have decided to publish early for testing purposes during development. You are free to check us out, but please note that this project is by no means complete, nor safe, and we **DO NOT** recommend using kux at this time. With that said, please feel free to check the library out and see where it's headed!

The short version:

kwaeri-ux (kux) is a robust and feature-rich user experience framework built with Material Design and kwaeri web developer tools (@kwaeri/web-developer-tools, or *kwdt* for short.).

If you like our software, please consider making a donation. Donations help greatly to maintain the Massively Modified network and continued support of our open source offerings:

[![Donate via PayPal.com](https://gitlab.com/mmod/kwaeri-user-experience/raw/master/images/mmod-donate-btn-2.png)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=YUW4CWCAABCU2)

## Table of Contents

* [The Implementation (about kux)](#the-implementation)
  * [Navigation](#navigation)
  * [Feedback](#feedback)
  * [Template Application & Documentation](#template-application)
  * [Developer Hype](#developers)
* [What Happened to the Original Kwaeri-UX](#what-happened-to-the-original-kwaeri-ux)
* [Using kux](#using-kux)
  * [Installation](#installation)
  * [Integration](#integration)
    * [JavaScript](#javascript)
* [How to Contribute Code](#how-to-contribute-code)
* [Other Ways to Contribute](#other-ways-to-contribute)
  * [Bug Reports](#bug-reports)
  * [Vulnerability Reports](#vulnerability-reports)
    * [Confidential Issues](#confidential-issues)
  * [Donations](#donations)

The core of kux is the Kwaeri Web Developer Tools (kwdt); a minimal set of core methods for JavaScript which enable rapid development of application logic. Building upon the core, kux provides a collection of methods which enable the development of innovative client-side applications by introducing a standardization of heavier processes. Continue reading to discover how to make the most out of kux!

## The Implementation

The core tool set provides potentially familiar methods; `.type()`, `.isNumber()`, `.empty()`, `.extend()`, `.each()`, `.hasClass()`, `.addClass()`, `.removeClass()`, `.ajax()`,  `.script()`  as well as a simple (though *powerful*) `.queue()` system.  We may very well add more tools to the collection as we continue to grow the project, though we've found that these tools more than suffice in helping  us to rapidly develop logic on the client-side of any application.

The user-experience library is where things get a bit interesting (to say the least)! We built upon the core tools to wrap intricate processes, and designed methods for easing the implementation of powerful features in a standardized way. While you can build a powerful synchronous application using kux, its designed especially for building asynchronous, responsive, modular applications. kux is specifically designed to ease building an SPA (Single Page Application) on the fly with our key points highlighted, yet to also handle scaling into something large and extensible, both while presenting a gorgeous, highly responsive and interactive user experience - proving to be extremely powerful and especially capable of encompassing modern, multi-facet applications.

For instance, the `.request()` method - contained within the user experience library - is designed to take as few arguments as *url* and *view*; The url is the target of the request, and the view is where to render the response. With some setup, the `.request()` method could run with just the target url provided, as a default view may be declared. The `.request()` method builds upon the ajax() method contained in the core tools, and provides an interface that encompasses several aspects of a user's experience while using an application. We didn't reinvent the wheel, but we did modify it - and with a purpose! Here's what you can expect:

### Navigation

As explained previously, a kux application is intended to be asynchronous. This means that you should hardly, if ever, see the browser refresh a page. The implementation for creating links and buttons are designed in a way that is non-obtrusive, and which fully supports asynchronous navigation in a simple and intuitive manner. How content is rendered and presented to a user is standardized with this in mind. Even the concepts of extensions and application management are convered in-depth - and are entirely supported such that an application built with kux may retain a high level of efficiency and organization.

### Feedback

The level of feedback a user can expect from a kux application truly depends on the developer. However, by making use of kux in your application, you can ensure a high level of responsiveness and provide user feedback such that the end user will have a lasting impression of your application. You tell kux what type of progress indicator to use, make use of our non-obtrusive implementation to map elements to our standardized set of facilities, and kux transforms your application to deliver a stunning user experience.

### Template Application

We're designing a template application which highlights features and demonstrates the standardization which we've established. The project is new and still under development, so please check back to know when things become available and for instructions on how to get started!

**Update**

The template application, [kwaeri-platform](https://gitlab.com/mmod/kwaeri-platform), is under development!  In time, instances of kwaeri-platform will run our websites: mmod.co and kwaeri.io - the former will be the homepage for the Massively Modified organization, while the latter of which will house documentation, additional technology, and products specific to the kwaeri trademark.

Until then, when you'll be able to browse all available documentation from the official home page, you can browse documentation baked with the kwaeri suite of technologies at the [Gitlab-hosted docs site](https://mmod.gitlab.io/kwaeri-user-experience/docs/documentation.html). Keep in mind that the documentation page is built using defaults - no customization to the theme - and uses the *article* flow. Examples of other types of content flow will be added in the future.

### Developers

The project is heavily geared for developers and is completely customizable. Installation enables a seamless work-flow, wrapping SCSS, TypeScript, and webpack/babel bundle build processes with Gulp.

## What happened to the original Kwaeri-UX

The original [Kwaeri-UX repository](https://gitlab.com/mmod/kwaeri-ux) still exists, and until further notice should still be used as a reference to the on-going changes stemming from the move-away of the legacy code. Information regarding the several components of the old code base can still be found within the README located there.

## Using kux

To use kux in your own project/application, you have several options.

### Installation

You can obtain kux a couple of different ways:

* Via npm with `npm install @kwaeri/user-experience` in terminal/command prompt.
* Via direct download in several available formats:
  * [.zip](https://gitlab.com/mmod/kwaeri-user-experience/-/archive/master/kwaeri-user-experience-master.zip)
  * [.tar.gz](https://gitlab.com/mmod/kwaeri-user-experience/-/archive/master/kwaeri-user-experience-master.tar.gz)
  * [.tar.bz2](https://gitlab.com/mmod/kwaeri-user-experience/-/archive/master/kwaeri-user-experience-master.tar.bz2)
  * [.tar](https://gitlab.com/mmod/kwaeri-user-experience/-/archive/master/kwaeri-user-experience-master.tar)

With either option, after obtaining the source, the production files can be found within the `dist` subdirectory.

### Integration

To use kux in your project/application, include the appropriate production files in your application's entry-point:

#### CSS

```html
<link rel="stylesheet" href="../path/to/kux/dist/css/kwaeri-ux-md.min.css">
```

#### JavaScript

```html
<script src="../path/to/kux/dist/js/mdc/kux-md-bundle.min.js"></script>
```

Source maps are provided, so feel free to use the minified JavaScript for development.

The kux and kwdt objects are pre-initialized and ready to go, load your application in the Chrome browser and you can start testing in the debug console immediately!

## How to Contribute Code

Our Open Source projects are always open to contribution. If you'd like to cocntribute, all we ask is that you follow the guidelines for contributions, which can be found at the [Massively Modified Wiki](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Contribute-Code)

There you'll find topics such as the guidelines for contributions; step-by-step walk-throughs for getting set up, [Coding Standards](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Coding-Standards), [CSS Naming Conventions](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/CSS-Naming-Conventions), and more.

## Other Ways to Contribute

There are other ways to contribute to the project other than with code. Consider [testing](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Test-Code) the software, or in case you've found an [Bug](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute/Bug-Reports) - please report it. You can also support the project monetarly through [donations](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Donations) via PayPal.

Regardless of how you'd like to contribute, you can also find in-depth information for how to do so at the [Massively Modified Wiki](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute)

### Bug Reports

To submit bug reports, request enhancements, and/or new features - please make use of the **issues** system baked-in to our source control project space at [Gitlab](https://gitlab.com/mmod/kwaeri-user-experience/issues)

You may optionally start an issue, track, and manage it via email by sending an email to our project's [support desk](mailto:incoming+mmod/kwaeri-user-experience@incoming.gitlab.com).

For more in-depth documentation on the process of submitting bug reports, please visit the [Massively Modified Wiki on Bug Reports](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute/Bug-Reports)

### Vulnerability Reports

Our Vulnerability Reporting process is very similar to Gitlab's. In fact, you could say its a *fork*.

To submit vulnerability reports, please email our [Security Group](mailto:security@mmod.co). We will try to acknowledge receipt of said vulnerability by the next business day, and to also provide regular updates about our progress. If you are curious about the status of your report feel free to email us again. If you wish to encrypt your disclosure email, like with gitlab - please email us to ask for our GPG Key.

Please refrain from requesting compensation for reporting vulnerabilities. We will publicly acknowledge your responsible disclosure, if you request us to do so. We will also try to make the confidential issue public after the vulnerability is announced.

You are not allowed, and will not be able, to search for vulnerabilities on Gitlab.com. As our software is open source, you may download a copy of the source and test against that.

#### Confidential Issues

When a vulnerability is discovered, we create a [confidential issue] to track it internally. Security patches will be pushed to private branches and eventually merged into a `security` branch. Security issues that are not vulnerabilites can be seen on our [public issue tracker](https://gitlab.com/mmod/kwaeri-user-experience/issues?label_name%5B%5D=Security).

For more in-depth information regarding vulnerability reports, confidentiality, and our practices; Please visit the [Massively Modified Wiki on Vulnerability](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute/Vulnerability-Reports)

### Donations

If you cannot contribute time or energy to neither the code base, documentation, nor community support; please consider making a monetary contribution which is extremely useful for maintaining the Massively Modified network and all the goodies offered free to the public.

[![Donate via PayPal.com](https://gitlab.com/mmod/kwaeri-user-experience/raw/master/images/mmod-donate-btn-2.png)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=YUW4CWCAABCU2)