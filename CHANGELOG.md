# Changelog

## Unreleased

### Added

* Added diverse entry points
  * Comprehensive - the entire offering.
  * Minimal - the minimal offering for web development
  * Game - the goodies that support a babylon-based game (unrefined and minimal)
  * Chat - the goodies that support a WebSocket-based chat (unrefined and minimal)
* Added central variables and mixins
  * For named partial variables and mixins (per partial)
* Added SASS to the development dependencies
  * Includes dart-sass support, enabling SASS modules. Needed for deriving from
    Material Components for web
* Added new scripts to `package.json` (`clean:styles`, `build:styles`, and
  `scripts:help`).

### Changed

* Updated dependencies:
  * Material Components updated to v11.0.0.
  * Webpack updated to v5.
  * Typescript updated to v4.3.5.
  * Charts.js updated to v3.5.1.
* Updated kux structure:
  * Now leverages SASS modules, dropped `@import` usage.
  * Every partial has its own named variables and mixins partial where applicable.
  * Theme is now more easily managed through named variable partials (base, etc).
  *
* Updated kux build:
  * Gulpfile now leverages SASS as default compiler for gulp-sass.
  * tsconfig now excludes `@material` component tests to remove errors and conflicts.
  * tsconfig now drops reference to `node` type library to resolve errors with
    setTimeout typing.
  * Webpack config partials now set `mode` (i.e. production or development).
  * Gulpfile now standardizes how tasks are named.
  * Package.json script names updated to reflect changes in gulpfile.
* Updated kux implementation:
  * Updated `Chart.js` required updated instantiation, as well as a call to
    `register` for registerables (i.e. scales, such as 'linear').
  * Updated use of mixins, as they evolved or were renamed due to SASS modules
    implementation that Material Design Components is now leveraging.
  * kux renamed to `kux_md--<entry>` (i.e. entry of comprehansive, minimal, etc).
  * Cleaned dependencies:
    * Updated Gulpfile, removing old imports, cleaning comments.

### Removed

* Removed `node-sass` from the build system, as a dependency
* Removed `@types/node` from the build system, as a dependency
  * Removed `node` typings reference from tsconfig
* Removed `@types/jest` from the build system, as a dependency
  * Removed `jest` typings reference from tsconfig
* Removed all other typings (i.e. `@types`) from package.json, as they were also
  unneccessary.

## 0.1.8 - 2018-06-14 18:57:22 EST

### Added

* Added smoothscroll-polyfill to the project from npm

### Changed

* Resolved #6, scrollToTarget now functions properly
  * scrollToX was changed to scrollToTop
  * scrollToTarget now uses scrollToTop and smoothscroll-polyfill to implement its intended feature(s)

### Removed

* scrollIt method was removed from the library in favor of scrollToTarget scrollToTop, and smoothscroll-polyfill.

## 0.1.7 - 2018-06-04 19:26:32 EST

### Added

* Added controls styling to the package build system (Closed Issue #3)
  * Textfield styling was added as the first of the conrols styles provided by kwaeri user experience

### Changed

* Resolved Issue #6, Ajax navigation crashes with CORS error

### Removed

## 0.1.6 - 2018-06-10 19:26:32 EST

### Added

### Changed

* Container is now centered by default.
  * Previously the container was left-aligned and caused a few complaints, understandably.

### Removed

## 0.1.5 - 2018-06-04 19:26:32 EST

### Added

* Added `CHANGELOG.md` for tracking changes to the project from release to release
  * Initial release added to the changelog
  * 0.1.1 notes added to the changelog as best as could be sorted
  * 0.1.2 notes added to the changelog as best as could be sorted
  * 0.1.3 notes added to the changelog as best as could be sorted
  * 0.1.4 notes added to the changelog

### Changed

* File versions bumped for release

### Removed

## 0.1.4 - 2018-06-04 16:30:22 EST

### Added

* Added dependency to gulp-bump-version for automated file-header version increment in gulpfile.babel.js

### Changed

* Updated its of the `README.md` which are static, but which are fully covered in the [documentation project wiki](https://gitlab.com/mmod/documentation/wikis/home)
  * Removed full text and replaced with references to the respective wiki pages
* `Gulpfile.babel.js` file updated to include 2 new tasks
  * `bump-file-versions` added for incrementing the version in file comment headers through out project
  * `bump-package-version` added for incrementing the version property in the `package.json` file
* Updated link to the Donations button so that it renders properly (old CDN site lost free domain associated with it)
  * Image is linked from this projects repository now

### Removed

* Chalk dev-dependency, managing colors ourselves now (see gulpfile.babel.js).

## 0.1.3 - 2018-05-30 11:53:00 EST

### Added

* Added index.js file to repository root for serving the library as a module for inclusion in other modules
* Added several sections to the `README.md` file
  * Coding Standards
  * CSS Naming Conventions
  * Bug Reports
  * Vulnerability Reports
* Documentation Added
  * Content for all methods added, such as the links in sidebar and fragments within the API section
* All Form Controls from Material Design added and intialized upon document load
  * Form Field
  * Floating Labels
  * Text Fields
  * Radio Buttons
  * Checkbox Buttons
  * Sliders
  * Icon Toggles
  * Line Ripples
  * Select Lists
  * Select Boxes
  * Switches
* New methods added for intializing controls
  * formControls
  * functionalControls

### Changed

* Controls method updated to call two sub methods instead when intializing UX Components in bulk
  * formControls
  * functionalControls

### Removed

## 0.1.2 - 2018-05-30 11:53:00 EST

### Added

* SCSS added which adds themeing and a document flow for our documentation site hosted with Gitlab
  * Document flow is similar to Material Designs, and is termed similarly, making using of HTML5 elements such as `section` and `article`
* Prism.js included in SCSS build now

### Changed

* Docs updated
  * Markup changed to implement the document flow in documentation.html
* Included standard property names to SCSS where previously there were only webkit based properties added through out SCSS fiiles
* Changed verbage used for variables for including kwaeri-user-experience library in library.js and others
  * From kwaeri-ux to kuxl
* Changed verbage used for variables for including kwaeri web developer tools in library.js and others
  * From kwdt to devTools
* Package.json now defines index.js as the package's main file

### Removed

## 0.1.1 - 2018-05-30 11:53:00 EST

### Added

* Documentation added in `README.md` file
  * How to contribute updated with Coding Standards full text
  * Bug Reports full-text added
  * Vulnerability Reports full-text added

### Changed

* Pipeline status reads from its own repository now instead of the old one

### Removed

## 0.1.0 - 2018-05-28 11:53:00 EST

### Initial Release
