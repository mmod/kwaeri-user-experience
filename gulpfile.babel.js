/*-----------------------------------------------------------------------------
 * @package:    Kwaeri web developer tools - Build Automaton
 * @author:     Richard B Winters
 * @copyright:  2015-2018 Massively Modified, Inc.
 * @license:    Apache-2.0
 * @version:    0.1.13
 *---------------------------------------------------------------------------*/


 // Absolutely declare this always.
 'use strict';


// INCLUDES
import gulp from 'gulp';                            // Obviously :)
import gts from 'gulp-typescript';                  // For compiling typescript, requires the typescript module also.
import gulpSass from 'gulp-sass';                   // Again, obviously!
import sass from 'sass';                            // We have to declare a default compiler for gulp-sass
import sassGlob from 'gulp-sass-glob';              // For special globbing support?
import watch from 'gulp-watch';                     // For Auto-Compilation/Transpilation upon save
import sourcemaps from 'gulp-sourcemaps';           // For generating JavaScript source maps
import webpack from 'webpack';                      // For bundling... ; ;
import webpackStream from 'webpack-stream';         // So gulp can manipulate the webpack bundling process
import uglifyes from 'uglify-es';                   // To minify es6+ JavaScript syntax
import composer from 'gulp-uglify/composer';        // Allows gulp-uglify to use an alternate uglify module (default is uglify-js)
import plumber from 'gulp-plumber';                 // For helping gulp manipulate webpack
import cssnano from 'gulp-cssnano';                 // To minify CSS
import rename from 'gulp-rename';                   // For adding suffixes/renaming files (we use it to name minified js files)
//import imagemin from 'gulp-imagemin';               // For compressing images and media items
//import cache from 'gulp-cache';                     // For caching compressed imagesS
import del from 'del';                              // Used by our clean process, for the most part -
import bump from 'gulp-bump-version';               // Used for version increment automation
import runsequence from 'run-sequence';             // For running tasks sequentially


// DEFINES
let defSass = gulpSass( sass );
let minify = composer( uglifyes, console );         // Replacing the uglify processor with one which supports es6+
let gtsProject = gts.createProject( 'tsconfig.json' );  // A preliminary for transpiling typescript using microsoft's compiler


// Parse command line arguments passed with script invocation,
// allows us to accept arguments to our gulpfile
const args =
(
    argList =>
    {
        let args = {}, i, option, thisOption, currentOption;

        for( i = 0; i < argList.length; i++ )
        {
            thisOption = argList[i].trim();

            option = thisOption.replace( /^\-+/, '' );

            if( option === thisOption )
            {
                // argument value
                if( currentOption )
                {
                    args[currentOption] = option;
                }

                currentOption = null;
            }
            else
            {
                currentOption = option;
                args[currentOption] = true;
            }
        }

        return args;
    }
)( process.argv );


// Check if we supplied arguments for updating the version of the project and its files
let bumpVersion         = args['bump-version'],
    toVersion           = args['version'],
    byType              = args['type'],
    bumpOptions         = ( bumpVersion ) ? { } : { type: 'patch' },
    projectBumpOptions  = ( bumpVersion ) ? { } : { type: 'patch', key: '"version": "' };

// If version was provided, overwrite the options accordingly
if( toVersion )
{
    bumpOptions = { version: toVersion };
    projectBumpOptions = bumpOptions;
    projectBumpOptions.key = '"version": "';
}

// If type was provided, overwrite the options accordingly
if( byType )
{
    bumpOptions = { type: byType };
    projectBumpOptions = bumpOptions;
    projectBumpOptions.key = '"version": "';
}


// Bump the version in our projects files (called manually, or through the build-release task)
gulp.task
(
    'bump:version--files',
    () =>
    {
        console.log( ' ', ( ( args.alt ) ? '[Build-alt]: ' : '[Build]: ' ) + 'Modifying version strings in project files...', ' ' );

        // Otherwise, by providing --bump-version only, you will increment the patch revision

        // Start with all the files using the typical key, then hit out package.json: 'dist/src/**/*.js',
        return gulp.src
        (
            [
                '**/*.ts',
                '**/*.js',
                '**/*.scss',
                '!dist/js/kwaeri-ux{,/**}',
                '!node_modules{,/**}'
            ],
            { base: './' }
        )
        .pipe( bump( bumpOptions ) )
        .pipe( gulp.dest( '.' ) );
    }
);


// Bump the version in our in our package.json file
gulp.task
(
    'bump:version--project',
    () =>
    {
        console.log( ' ', ( ( args.alt ) ? '[Build-alt]: ' : '[Build]: ' ) + 'Modifying version string in package.json...', ' ' );

        // Target out package.json:
        return gulp.src
        (
            'package.json',
            { base: './' }
        )
        .pipe( bump( projectBumpOptions ) )
        .pipe( gulp.dest( '.' ) );
    }
);


// Compile Typescript files
gulp.task
(
    'compile:typescript',
    () =>
    {
        console.log( ' ', ( ( args.alt ) ? '[Build-alt]: ' : '[Build]: ' ) + 'Compiling Typescript sources into JavaScript...', ' ' );

        return gtsProject.src() // This line pulls the 'src' from the tsconfig.json file
        .pipe( gtsProject() )
        .js.pipe( gulp.dest( 'dist/js' ) );
        //.js.pipe( gulp.dest( 'dist/js/src' ) );
    }
);


// Compile Typescript files
gulp.task
(
    'generate:typescript-declarations',
    () =>
    {
        console.log( ' ', ( ( args.alt ) ? '[Build-alt]: ' : '[Build]: ' ) + 'Generating Typescript declarations...', ' ' );

        return gtsProject.src() // This line pulls the 'src' from the tsconfig.json file
        .pipe( gtsProject() )
        .dts.pipe( gulp.dest( 'dist/js' ) );
        //.dts.pipe( gulp.dest( 'dist/js/src' ) );
    }
);

// Compiles SCSS files
gulp.task
(
    'compile:scss',
    () =>
    {
        if( args.dev )
        {
            //return gulp.src( 'scss/kwaeri-ux-md.scss' )
            return gulp.src( 'scss/*.scss' )
            .pipe(plumber())
            .pipe( sourcemaps.init() )
            .pipe( sassGlob() )
            .pipe
            (
                defSass( { style: 'compressed', includePaths: ['node_modules'] } ).on( 'error', defSass.logError )
            )
            .pipe( sourcemaps.write( '.' ) )  // Remove '.' for inline scss sourcemaps
            .pipe( gulp.dest( 'dist/css' ) );
        }

        //return gulp.src( 'scss/kwaeri-ux-md.scss' )
        return gulp.src( 'scss/*.scss' )
        .pipe(plumber())
        .pipe( sassGlob() )
        .pipe
        (
            defSass( { style: 'compressed', includePaths: ['node_modules'] } ).on( 'error', defSass.logError )
        )
        .pipe( gulp.dest( 'dist/css' ) );
    }
);


// Watches for changes in TS & SCSS files
gulp.task
(
    'watch',
    () =>
    {
        console.log( ' ', ( ( args.alt ) ? '[Build-alt]: ' : '[Build]: ' )  + 'Watching for changes to SCSS source...', ' ' );

        gulp.watch( 'src/**/*.ts', ['compile-typescript'] );
        gulp.watch( 'scss/**/*.scss', ['compile-sass'] );       // We'll watch all scss files for changes, although we only build
                                                                // with the main scss file as a source target.
    }
);


// Bundle JS files with Webpack for use in a browser
gulp.task
(
    'transpile:library',
    () =>
    {
        console.log( ' ', ( ( args.alt ) ? '[Build-alt]: ' : '[Build]: ' ) + 'Transpiling and Bundling MDC & Kwaeri JavaScript using Webpack...', ' ' );

        return gulp.src
        (
            'src/library.js'
        )
        .pipe( plumber() )  // Prevent stop on error
        .pipe( webpackStream( ( ( args.dev ) ? require( './webpack/webpack.dev.config' ) : require( './webpack/webpack.prod.config' ) ) ), webpack )
        .pipe( gulp.dest( 'dist/js/kwaeri-ux' ) );
    }
);


// Optimzes JS files
gulp.task
(
    'optimize:js',
    () =>
    {
        console.log( ' ', ( ( args.alt ) ? '[Build-alt]: ' : '[Build]: ' ) + 'Minifying test JavaScript source files...', ' ' );

        //return gulp.src( [ 'dist/js/src/kwdt.js', 'dist/js/test/test.js' ] )
        return gulp.src( [ 'dist/js/**/*.js', '!dist/js/kwaeri-ux/*.js' ] )
        .pipe( sourcemaps.init() )
        .pipe( minify() )
        .on('error', function (err) { gutil.log(gutil.colors.red('[Error]'), err.toString()); })
        .pipe( rename( { suffix: '.min' } ) )
        .pipe( sourcemaps.write( '.', { includeContent: false, sourceRoot: '' } ) )
        .pipe( gulp.dest( 'dist/js' ) );
    }
);



// Copy fonts to dist
gulp.task
(
    'distribute:fonts',
    () =>
    {
        return gulp.src( [ 'fonts/**/*.ttf', 'fonts/**/*.woff', 'fonts/**/*.woff2' ] )
        .pipe( gulp.dest( 'dist/fonts' ) );
    }
);


// Optimzes CSS files
gulp.task
(
    'optimize:css',
    () =>
    {
        console.log( ' ', ( ( args.alt ) ? '[Build-alt]: ' : '[Build]: ' ) + 'Minifying CSS source files...', ' ' );

        if( args.dev )
        {
            return gulp.src( 'dist/css/*.css' )
            .pipe( sourcemaps.init() )
            .pipe( cssnano() )
            .pipe( sourcemaps.write( '.' ) )
            .pipe( rename( { suffix: '.min' } ) )
            .pipe( gulp.dest( 'dist/css' ) );
        }

        return gulp.src( 'dist/css/*.css' )
        .pipe( cssnano() )
        .pipe( rename( { suffix: '.min' } ) )
        .pipe( gulp.dest( 'dist/css' ) );
    }
);


// Cleans the dist directory (removes it).
gulp.task
(
    'clean:styles',
    ( done ) =>
    {
        console.log( ' ', ( ( args.alt ) ? '[Build-alt]: ' : '[Build]: ' )  + 'Cleaning distribution styles...', ' ' );

        let deleteList = ( !args.alt ) ? 'dist/css/**/*' : 'dist/css/**/*';
        del.sync
        (
            deleteList,
            { force: true }
        );

        // While typescript compiler can just return its process,
        // for some reason del.sync() needs to trigger async
        // completion (ensure you provide the argument to the
        // anon function so that it's available):
        done();
    }
);


// Cleans the dist directory (removes it).
gulp.task
(
    'clean:distribution',
    ( done ) =>
    {
        console.log( ' ', ( ( args.alt ) ? '[Build-alt]: ' : '[Build]: ' )  + 'Cleaning distribution...', ' ' );

        let deleteList = ( !args.alt ) ? 'dist' : 'dist';
        del.sync
        (
            deleteList,
            { force: true }
        );

        // While typescript compiler can just return its process,
        // for some reason del.sync() needs to trigger async
        // completion (ensure you provide the argument to the
        // anon function so that it's available):
        done();
    }
);


// Cleans the dist directory (removes it).
gulp.task
(
    'build:styles',
    gulp.series
    (
        'clean:styles',
        'compile:scss',
        'optimize:css',
        ( done ) =>
        {
            done();
        }
    )
);


// Cleans the dist directory (removes it).
gulp.task
(
    'build:release',
    gulp.series
    (
        'clean:distribution',
        'bump:version--project',
        'bump:version--files',
        'distribute:fonts',
        'compile:scss',
        'optimize:css',
        'compile:typescript',
        'generate:typescript-declarations',
        'transpile:library',
        'optimize:js',
        ( done ) =>
        {
            done();
        }
    )
);


// Cleans the dist directory (removes it).
gulp.task
(
    'default', // build
    gulp.series
    (
        'clean:distribution',
        'distribute:fonts',
        'compile:scss',
        'optimize:css',
        'compile:typescript',
        'generate:typescript-declarations',
        'transpile:library',
        'optimize:js',
        ( done ) =>
        {
            done();
        }
    )
);


