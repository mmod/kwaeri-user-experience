// INCLUDES
import merge from 'webpack-merge';
import baseConfig from './webpack.config';
import uglifyJsPlugin from 'uglifyjs-webpack-plugin';


module.exports = merge
(
    baseConfig,
    {
        mode: "development",
        devtool: 'source-map',
        plugins:
        [
            new uglifyJsPlugin
            (
                {
                    sourceMap: true
                }
            )
        ]
    }
);