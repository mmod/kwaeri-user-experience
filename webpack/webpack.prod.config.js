// INCLUDES
import merge from 'webpack-merge';
import baseConfig from './webpack.config';
import uglifyJsPlugin from 'uglifyjs-webpack-plugin';


module.exports = merge
(
    baseConfig,
    {
        mode: "production",
        devtool: false,
        plugins:
        [
            new uglifyJsPlugin
            (
                {
                    sourceMap: false
                }
            )
        ]
    }
);