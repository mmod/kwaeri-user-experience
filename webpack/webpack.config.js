/*-----------------------------------------------------------------------------
 * @package:    kwaeri-user-experience
 * @author:     Richard B Winters
 * @copyright:  2021 Massively MOdified, Inc.
 * @license:    Apache-2.0 <http://www.apache.org/licenses/LICENSE-2.0>
 * @version:    0.1.0
 *---------------------------------------------------------------------------*/


// INCLUDES
import * as path from 'path';
import glob from 'glob';


module.exports =
{
    output:
    {
      filename: 'kux-md-bundle.min.js',
      sourceMapFilename: 'kux-md-bundle.min.js.map'
    },
    module:
    {
      rules:
      [
        {
          test: /\.(js|jsx)$/,
          exclude: /(node_modules)/,
          loader: 'babel-loader',
          query:
          {
            presets:
            [
              ['@babel/preset-env'],
            ],
          },
        },
        {
          test: /\.scss$/,
          loader: 'sass-loader',
          options: {
            includePaths: ['node_modules', 'node_modules/@material/*']
              .map((d) => path.join(__dirname, d))
              .map((g) => glob.sync(g))
              .reduce((a, c) => a.concat(c), [])
          }
        }
      ],
    }
};